.. -*- coding: utf-8 -*-
.. :Project:   CTA
.. :Created:   lun 09 nov 2015 10:41:32 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: Copyright (C) 2015 Lele Gaifax
..

===========================
 Carrom, tecnica e abilità
===========================

Italian translation of “Carrom, Skill and Technique” by Arun Deshpande
======================================================================

:authors: Arun Deshpande, Lele Gaifax <lele@metapensiero.it>
:License: GNU GPLv3 e GNU FDL

This repository contains the sources of the italian translation of the book.

To produce the PDF, you will need `LyX`_, `XeTeX`_, `Python`_ and `PyX`_.

.. _lyx: http://www.lyx.org/
.. _xetex: http://xetex.sourceforge.net/
.. _python: http://www.python.org/
.. _pyx: http://pyx.sourceforge.net/
