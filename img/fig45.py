# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 45
# :Created:   Sun Dec 28 11:46:34 2008
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2008-2015 Lele Gaifax
#

from board import (
    BORDER_RIGHT,
    CarromBoard,
    POCKET_D,
    )

cb = CarromBoard()

q = cb.addQueen(45,51)
b1 = cb.addBlackCM(q,-25)
b2 = cb.addBlackCM(54,40)
w1 = cb.addWhiteCM(55,35)
s1 = cb.addStriker(6.7,"A")

hit3 = q.traceTo(POCKET_D)
hit2 = b1.hitPoint(-35)
hit1 = b2.traceTo(BORDER_RIGHT, (64,71), (50,55))

s1.traceTo(hit1,hit2)

cb.exportEps("fig45")
