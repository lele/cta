# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 18: Bomb examples
# :Created:   Sat May 1 17:17:07 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BOARD_WIDTH,
    BORDER_BASE,
    BORDER_LEFT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(28, 12)
w2 = cb.addWhiteCM(57, 3)

s1 = cb.addStriker(4.6, 'A')
s2 = cb.addStriker(7, 'B')

hit1 = w1.traceTo(POCKET_BASE_LEFT)
s1.traceTo(hit1, BORDER_LEFT, (25,BOARD_WIDTH-(4.13/2)), (45,50))

hit2 = w2.traceTo(POCKET_BASE_RIGHT)
s2.traceTo(BORDER_BASE, hit2, BORDER_RIGHT, (55, 52))

cb.exportEps('fig18')
