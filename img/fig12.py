# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 12: Simple rebound examples
# :Created:   Sun Feb 29 00:43:36 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from pyx import style
from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(5.5,71)
b2 = cb.addBlackCM(29,71)
w1 = cb.addWhiteCM(5.5,14)
s1 = cb.addStriker(1, 'A')
hit1 = w1.traceTo(POCKET_BASE_LEFT)
s1.traceTo(BORDER_FRONT, hit1)

w2 = cb.addWhiteCM(32, 62)
s2 = cb.addStriker(7, 'C')
hit2 = w2.traceTo(POCKET_BASE_LEFT)
s2.traceTo(BORDER_FRONT, hit2)

b3 = cb.addBlackCM(62,25)
b4 = cb.addBlackCM(63,14.5)
w3 = cb.addWhiteCM(b4,45,4)

s3 = cb.addStriker(8,'B')
hit3 = b3.hitPoint(45)
hit4 = w3.traceTo(POCKET_BASE_RIGHT)
s3.traceTo(BORDER_FRONT, (74-s3.radius/2,55), hit3, hit4)

s3.linestyle = style.linestyle.dotted
hit5 = b4.hitPoint(50)
w3.linestyle = style.linestyle.dotted
hit6 = w3.traceTo(hit5, POCKET_BASE_RIGHT)
s3.traceTo(BORDER_FRONT, (74-s3.radius/2,44), hit6)

s4 = cb.addStriker(9.1,'D')

s4.traceTo(BORDER_FRONT, (74-s3.radius/2,61), hit4)

cb.exportEps("fig12")
