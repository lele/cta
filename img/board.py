# -*- coding: utf-8 -*-
# :Project:   CTA -- Sample drawings
# :Created:   Fri Dec  5 09:58:00 2003
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2003-2015 Lele Gaifax
#

"""Modulo per la generazione dei disegni di esempio del libro di Arun.

Questo modulo implementa alcune classi che rappresentano un tavolo da
Carrom e le pedine usate nel gioco, al fine di semplificare al massimo
la generazione dei disegni del libro "Carrom, Tecnica e Abilità".

Eseguendolo direttamente, si otterrà un disegno esplicativo.
"""

from pyx import unit, canvas, style, deco

# Lavoriamo in millimetri, con righe di larghezza al 10%
unit.set(wscale=0.1, defaultunit="mm")

BOARD_WIDTH = 74.0
"Dimensione del tavolo da gioco"

_POCKET_RADIUS = 2.225
"Raggio di ciascuna buca"

POCKET_BASE_LEFT = POCKET_A = (_POCKET_RADIUS, _POCKET_RADIUS)
"Buca A, in basso a sinistra"

POCKET_BASE_RIGHT = POCKET_B = (BOARD_WIDTH - _POCKET_RADIUS, _POCKET_RADIUS)
"Buca B, in basso a destra"

POCKET_FRONT_RIGHT = POCKET_C = (BOARD_WIDTH - _POCKET_RADIUS, BOARD_WIDTH - _POCKET_RADIUS)
"Buca C, in alto a destra"

POCKET_FRONT_LEFT = POCKET_D = (_POCKET_RADIUS, BOARD_WIDTH -_POCKET_RADIUS)
"Buca D, in alto a sinistra"

BORDER_BASE = BORDER_A = "A"
"Sponda di base"

BORDER_RIGHT = BORDER_B = "B"
"Sponda laterale destra"

BORDER_FRONT = BORDER_C = "C"
"Sponda opposta"

BORDER_LEFT = BORDER_D = "D"
"Sponda laterale sinistra"

_STRIKER_RADIUS = 4.13/2.0
_CCIRCLE_RADIUS = 3.18/2.0
_ICIRCLE_RADIUS = 16.19/2.0
_OCIRCLE_RADIUS = 21.27/2.0
_CMEN_RADIUS = 3.18/2.0
_BLINES_DIST = 3.18
_BLINES_LENGTH = 47.0 - _BLINES_DIST
_BLINES_OFFSET = 10.15


class CarromBoard:
    """Rappresentazione di un tavolo da Carrom.

       Questa classe si preoccupa di disegnare un tavolo da Carrom, al
       fine di generare degli esempi di tiri.
    """

    def __init__(self):
        """Costruttore del CarromBoard."""

        self.canvas = canvas.canvas()
        self._draw()

    def exportEps(self, filename):
        """Esporta il disegno in formato EPS."""

        self.canvas.writeEPSfile(filename)

    def addBlackCM(self, *args):
        """Aggiungi una pedina nera."""

        return BlackCarromMan(self.canvas, *args)

    def addWhiteCM(self, *args):
        """Aggiungi una pedina bianca."""

        return WhiteCarromMan(self.canvas, *args)

    def addQueen(self, *args):
        """Aggiungi una Regina."""

        return Queen(self.canvas, *args)

    def addStriker(self, pos, label):
        """Aggiungi uno Striker in posizione X sulla linea di base,
           etichettandolo con `label`"""

        offs = (BOARD_WIDTH - _BLINES_LENGTH) / 2
        x = offs + (_BLINES_LENGTH/10.0*pos)
        return Striker(self.canvas, x, _BLINES_OFFSET+_BLINES_DIST/2, label)

    def _draw(self):
        self._drawBorder()
        self._drawPockets()
        self._drawBaseLines()
        self._drawDecorations()
        self._drawCenterCircles()

    def _drawPockets(self):
        from pyx import path

        for x, y in [POCKET_A, POCKET_B, POCKET_C, POCKET_D]:
            self.canvas.stroke(path.circle(x, y, _POCKET_RADIUS))

    def _drawBorder(self):
        from pyx import path
        from pyx.path import moveto, lineto, arc

        border = path.path(moveto(_POCKET_RADIUS, 0),
                           lineto(BOARD_WIDTH-_POCKET_RADIUS, 0),
                           arc(POCKET_B[0], POCKET_B[1],
                               _POCKET_RADIUS, 270, 0),
                           lineto(BOARD_WIDTH, BOARD_WIDTH-_POCKET_RADIUS),
                           arc(POCKET_C[0], POCKET_C[1],
                               _POCKET_RADIUS, 0, 90),
                           lineto(_POCKET_RADIUS, BOARD_WIDTH),
                           arc(POCKET_D[0], POCKET_D[1],
                               _POCKET_RADIUS, 90, 180),
                           lineto(0, _POCKET_RADIUS),
                           arc(POCKET_A[0], POCKET_A[1],
                               _POCKET_RADIUS,
                               180, 270))

        self.canvas.stroke(border, [style.linewidth.THICK])

    def _drawBaseLines(self):
        from pyx import path, trafo
        from pyx.path import moveto, lineto, arc

        offs = (BOARD_WIDTH - _BLINES_LENGTH) / 2

        # Disegna il profilo della linea di base
        blines = path.path(moveto(offs, _BLINES_OFFSET),
                           lineto(offs+_BLINES_LENGTH, _BLINES_OFFSET),
                           arc(offs+_BLINES_LENGTH,
                               _BLINES_OFFSET+_BLINES_DIST/2,
                               _BLINES_DIST/2, 270.0, 90.0),
                           lineto(offs, _BLINES_OFFSET+_BLINES_DIST),
                           arc(offs, _BLINES_OFFSET+_BLINES_DIST/2,
                               _BLINES_DIST/2, 90.0, 270.0))

        d = BOARD_WIDTH - _BLINES_OFFSET*2 - _BLINES_DIST

        #blines = path.normpath(blines)

        # Aggiungi una copia traslata sull'altro lato
        blines += blines.transformed(trafo.translate(0, d))

        # Raddoppia il tutto, aggiungendo una copia ruotata di 90
        blines += blines.transformed(trafo.rotate(90, BOARD_WIDTH/2,
                                                  BOARD_WIDTH/2))

        # Disegna i cerchietti tra le linee di base
        self.canvas.stroke(blines)
        self.canvas.stroke(path.circle(_BLINES_OFFSET+_BLINES_DIST,
                                       _BLINES_OFFSET+_BLINES_DIST,
                                       1.27/2.0))
        self.canvas.stroke(path.circle(BOARD_WIDTH-_BLINES_OFFSET-_BLINES_DIST,
                                       _BLINES_OFFSET+_BLINES_DIST,
                                       1.27/2.0))
        self.canvas.stroke(path.circle(_BLINES_OFFSET+_BLINES_DIST,
                                       BOARD_WIDTH-_BLINES_OFFSET-_BLINES_DIST,
                                       1.27/2.0))
        self.canvas.stroke(path.circle(BOARD_WIDTH-_BLINES_OFFSET-_BLINES_DIST,
                                       BOARD_WIDTH-_BLINES_OFFSET-_BLINES_DIST,
                                       1.27/2.0))

        # Disegna i cerchi di base
        decor = path.circle(offs, _BLINES_OFFSET+_BLINES_DIST/2, 1.27)
        #decor = path.normpath(decor)

        decor += decor.transformed(trafo.translate(0, d))
        decor += decor.transformed(trafo.translate(_BLINES_LENGTH, 0))
        decor += decor.transformed(trafo.rotate(90, BOARD_WIDTH/2,
                                                BOARD_WIDTH/2))
        #self.canvas.fill(decor, color.rgb.red)
        self.canvas.stroke(decor, [style.linewidth.THIck])

    def _drawDecorations(self):
        from pyx import path, trafo
        from pyx.path import arc

        # Disegna la decorazione, fatta da una freccia
        decor = path.line(_POCKET_RADIUS+5.08/1.4142,
                          _POCKET_RADIUS+5.08/1.4142,
                          _POCKET_RADIUS+(5.08+26.67)/1.4142,
                          _POCKET_RADIUS+(5.08+26.67)/1.4142)
        self.canvas.stroke(decor, [deco.barrow.small])

        # Disegna le altre, ruotando di volta in volta di 90 gradi
        for i in range(3):
            decor = decor.transformed(trafo.rotate(90, BOARD_WIDTH/2,
                                               BOARD_WIDTH/2))
            self.canvas.stroke(decor, [deco.barrow.small])

        # Disegna gli archi di cerchio in cima alla freccia
        decor = path.path(arc(_POCKET_RADIUS+(5.08+26.67-6.35/2)/1.4142,
                              _POCKET_RADIUS+(5.08+26.67-6.35/2)/1.4142,
                              6.35/2,
                              270, 180))
        self.canvas.stroke(decor, [deco.barrow.Small, deco.earrow.Small])

        # Disegna gli altri, ruotando di 90 gradi ogni colpo
        for i in range(3):
            decor = decor.transformed(trafo.rotate(90, BOARD_WIDTH/2,
                                                   BOARD_WIDTH/2))
            self.canvas.stroke(decor, [deco.barrow.Small, deco.earrow.Small])

    def _drawCenterCircles(self):
        from pyx import path

        self.canvas.stroke(path.circle(BOARD_WIDTH/2,
                                       BOARD_WIDTH/2,
                                       _CCIRCLE_RADIUS),
                           [style.linewidth.THIck])
        self.canvas.stroke(path.circle(BOARD_WIDTH/2,
                                       BOARD_WIDTH/2,
                                       _ICIRCLE_RADIUS))
        self.canvas.stroke(path.circle(BOARD_WIDTH/2,
                                       BOARD_WIDTH/2,
                                       _OCIRCLE_RADIUS))


class HitPoint:
    """Rappresenta un punto d'impatto, con una certa angolazione."""

    def __init__(self, x, y, cx, cy):
        """Costruttore di un HitPoint.

        Inizializza un'istanza, rappresentante un punto d'impatto
        (x,y) con angolazione data dai coefficienti cx e cy che
        danno la direzione dell'urto.
        """

        self.x = x
        self.y = y
        self.cx = cx
        self.cy = cy

    def mark(self, canvas):
        """Evidenzia il punto di contatto con un ostacolo."""

        from pyx import path, color

        otherx, othery = self.pointAtDistance(1.5)
        m = path.line(self.x, self.y, otherx, othery)
        canvas.stroke(m, [deco.barrow.Small, color.rgb.blue])

    def pointAtDistance(self, distance):
        """Determina la posizione di un punto a una certa distanza
           da quello di contatto, secondo l'angolazione di incidenza.
        """

        return (self.x - distance*self.cx, self.y - distance*self.cy)

class CarromMan:
    """Rappresentazione di una pedina generica.

       Questa classe fa da prototipo di una pedina, che sa muoversi
       sul tavolo lasciando una traccia.
    """

    radius = _CMEN_RADIUS
    "Raggio della pedina"

    linestyle = style.linestyle.dashed
    "Stile di linea con cui lasciare traccia degli spostamenti"

    def __init__(self, c, *args):
        """Costruttore di una pedina.

           Il primo parametro è il canvas dove disegnare, i successivi
           indicano il centro della pedina, o come coppia di valori
           X,Y, oppure indicando un'altra pedina, un angolo e
           facoltativamente una distanza (di default pari alla somma
           dei due raggi) per posizionare una pedina relativamente a
           un'altra::

             >>> bianca = cb.addWhiteCM(15,25)
             >>> nera = cb.addBlackCM(bianca, 60, 12)
        """

        from pyx import text

        self.canvas = c
        args = list(args)
        center = self._computeCenter(args)
        self.x, self.y = center
        self._draw()
        if args:
            from math import sin, cos, pi
            label = args.pop(0)
            if args:
                angle = args.pop(0)*pi/180
            else:
                angle = 30.0*pi/180
            dist = self.radius*1.9
            self.canvas.text(self.x + cos(angle)*dist,
                             self.y + sin(angle)*dist,
                             r"\bf %s" % label,
                             [text.halign.center, text.vshift.middlezero])

    def _computeCenter(self, args):
        """Se la posizione è data relativamente a un'altra pedina,
           determina le coordinate del centro, altrimenti ritorna
           le coordinate date.
        """

        if isinstance(args[0], CarromMan):
            from math import sin, cos, pi
            cm = args.pop(0)
            angle = args.pop(0)*pi/180
            if args and not isinstance(args[0], str):
                dist = args.pop(0)
            else:
                dist = cm.radius
            dist += self.radius
            return (cm.x + cos(angle)*dist, cm.y + sin(angle)*dist)
        else:
            return (args.pop(0), args.pop(0))

    def hitPoint(self, angle):
        """Determina il punto d'impatto sulla circonferenza a una
           certa angolazione.
        """

        from math import sin, cos, pi
        rad = angle*pi/180
        x, y = self.x+cos(rad)*self.radius, self.y+sin(rad)*self.radius
        deltax = self.x - x
        deltay = self.y - y
        return HitPoint(x, y, deltax / self.radius, deltay / self.radius)


    def bouncePoint(self, p1, p2, border):
        """Calcola il punto di impatto con uno dei bordi per poi
           rimbalzare in una certa direzione. P1 e P2 possono anche
           essere indicati come punto di contatto con un altro
           oggetto, il valore ritornato da .traceTo(), nel qual caso
           verrà aggiustato per tener conto del raggio della pedina.
        """

        if isinstance(p1, HitPoint):
            x1,y1 = p1.pointAtDistance(self.radius)
        else:
            x1,y1 = p1

        if isinstance(p2, HitPoint):
            x2,y2 = p2.pointAtDistance(self.radius)
        else:
            x2,y2 = p2

        if border==BORDER_A or border==BORDER_C:
            if x1 == x2:
                x = x1
                y = y1
            else:
                if border==BORDER_C:
                    y = BOARD_WIDTH-self.radius
                else:
                    y = self.radius

                x = ((x2+x1)*y - y1*x2 - y2*x1) / (2*y - y1 - y2)
        else:
            if y1 == y2:
                x = x1
                y = y1
            else:
                if border==BORDER_B:
                    x = BOARD_WIDTH-self.radius
                else:
                    x = self.radius

                y = ((y1+y2)*x - x1*y2 - y1*x2) / (2*x - x1 - x2)

        return x,y

    def traceTo(self, *points):
        """Sposta la pedina lasciando traccia.

           Il metodo accetta una sequenza di stringhe, di HitPoint o
           di tuple: nel caso di una stringa, deve trattarsi di una
           lettera "A", "B", "C" o "D", a indicare rispettivamente
           la sponda di base, quella destra, quella opposta e quella
           sinistra dove far rimbalzare la pedina per raggiungere il
           punto successivo; un HitPoint rappresenta un punto di
           collisione, che verrà corretto con il diametro di questo
           oggetto; altrimenti la tupla rappresenta una coppia di
           coordinate del punto verso il quale spostare la pedina
           lasciando una traccia visibile.

           Ritorna un HitPoint.

             >>> hit = whitecm.traceTo(POCKET_C)
             >>> striker.traceTo(hit, (25,BOARD_WIDTH), POCKET_A)
        """

        from pyx import path, unit
        from pyx.path import moveto_pt, lineto

        last = None
        next = None
        exitp = None

        i = 0
        while i < len(points):
            p = points[i]

            # Solo per il primo punto...

            if not exitp:
                # Estrai il primo punto, per individuare da dove partire
                # a tracciare la riga, dal bordo della pedina

                # Se si tratta di una lettera, indica che si desidera un
                # rimbalzo su una certa sponda per raggiungere il
                # punto successivo

                if type(p)==type(''):
                    i += 1
                    border = p
                    next = points[i]
                    if isinstance(next, HitPoint):
                        next.mark(self.canvas)
                        next = next.pointAtDistance(self.radius)
                    p = self.bouncePoint((self.x,self.y),next,border)
                elif isinstance(p, HitPoint):
                    # Se si tratta di un hitpoint, aggiusta il punto
                    # d'arrivo tenendo conto del raggio di questo
                    # oggetto
                    p.mark(self.canvas)
                    p = p.pointAtDistance(self.radius)

                assert(type(p)==type(()) and len(p)==2)

                # Determina il punto di "uscita" del primo segmento
                line = path.line(self.x, self.y, p[0], p[1])
                c = path.circle(self.x, self.y, self.radius)
                sp = line.intersect(c)
                exitp = line.at(sp[0][0])

                # Determina il punto di impatto necessario per far andare
                # la pedina in quella direzione
                deltax = unit.tomm(exitp[0]) - self.x
                deltay = unit.tomm(exitp[1]) - self.y
                hitx = self.x - deltax
                hity = self.y - deltay

                # Traccia il primo tratto del percorso
                trace = path.path(moveto_pt(unit.topt(exitp[0]),
                                            unit.topt(exitp[1])),
                                  lineto(p[0], p[1]))
                if next:
                    p = next

            # In ogni caso, se il punto è una lettera, allora indica su
            # che sponda si vuol far rimbalzare la pedina per raggiungere
            # il punto successivo

            if type(p) == type(''):
                i += 1
                border = p
                next = points[i]
                if isinstance(next, HitPoint):
                    next.mark(self.canvas)
                    next = next.pointAtDistance(self.radius)
                p = self.bouncePoint(last, next, border)
                trace.append(lineto(p[0],p[1]))
                p = next
            elif isinstance(p, HitPoint):
                p.mark(self.canvas)
                p = p.pointAtDistance(self.radius)

            assert(type(p)==type(()) and len(p)==2)

            trace.append(lineto(p[0],p[1]))

            last = p
            i += 1

        self.canvas.stroke(trace,
                           [self.linestyle, style.linewidth.THICK,
                            deco.earrow.small])

        return HitPoint(hitx, hity,
                        deltax / self.radius, deltay / self.radius)

    def hit(self, obstacle, *points):
        """Urta contro un ostacolo, e prosegui per i punti successivi.

        L'ostacolo può essere specificato sia come hitpoint che come
        tupla (pedina, angolo).
        """

        if isinstance(obstacle, HitPoint):
            first = (obstacle.x + self.radius * obstacle.cx,
                     obstacle.y + self.radius * obstacle.cy)
        else:
            other, angle = obstacle
            first = self._computeCenter( [other, angle, other.radius] )

        return self.traceTo(first, *points)


class WhiteCarromMan(CarromMan):
    """Una pedina bianca"""

    def _draw(self):
        """Disegna una pedina bianca."""

        from pyx import path, trafo, color

        #cm = path.normpath(path.circle(self.x, self.y, self.radius))
        cm = path.circle(self.x, self.y, self.radius)
        self.canvas.fill(cm, [color.gray.white])
        cm += cm.transformed(trafo.scale(0.5, x=self.x, y=self.y))
        cm += cm.transformed(trafo.scale(0.5, x=self.x, y=self.y))

        self.canvas.stroke(cm, [style.linewidth.THICK])


class BlackCarromMan(CarromMan):
    """Una pedina nera"""

    def _draw(self):
        """Disegna una pedina nera."""

        from pyx import path, trafo, color

        #cm = path.normpath(path.circle(self.x, self.y, self.radius))
        cm = path.circle(self.x, self.y, self.radius)
        self.canvas.fill(cm, [color.gray.black])

        cm = cm.transformed(trafo.scale(0.5, x=self.x, y=self.y))
        cm += cm.transformed(trafo.scale(0.5, x=self.x, y=self.y))
        self.canvas.stroke(cm, [color.grey.white, style.linewidth.THICK])


class Queen(CarromMan):
    """La regina"""

    def _draw(self):
        """Disegna la Regina."""

        from pyx import path, color
        from pyx.path import moveto, rlineto, closepath

        q = path.circle(self.x, self.y, self.radius)
        self.canvas.fill(q, [color.gray.white])
        self.canvas.stroke(q, [style.linewidth.THICK])

        p = path.path(moveto(self.x-1, self.y+0.7),
                      rlineto(0.5,-0.5),
                      rlineto(0.5,0.5),
                      rlineto(0.5,-0.5),
                      rlineto(0.5,0.5),
                      rlineto(-0.3, -1.5),
                      rlineto(-1.4, 0),
                      closepath())

        self.canvas.fill(p, [color.rgb.red])
        self.canvas.stroke(p, [style.linewidth.THIck])


class Striker(CarromMan):
    """Rappresentazione di uno Striker.

       Lo Striker è sostanzialmente una pedina un po' più grande,
       che ai fini degli esempi riporta una lettera "A", "B"...
    """

    radius = _STRIKER_RADIUS
    linestyle = style.linestyle.dashdotted

    def __init__(self, c, x, y, l):
        """Costruttore di uno Striker."""

        self.canvas = c
        self.x = x
        self.y = y
        self.label = l
        self._draw()

    def _draw(self):
        """Disegna lo Striker."""

        from pyx import path, text, color

        c = path.circle(self.x, self.y, self.radius)
        self.canvas.fill(c, [color.gray.white])
        self.canvas.text(self.x, self.y, r"\bf %s" % self.label,
                         [text.halign.center, text.vshift.middlezero])
        self.canvas.stroke(c, [style.linewidth.THICK])


if __name__ == '__main__':
    # Crea un tavolo da Carrom
    cb = CarromBoard()

    # aggiungi una Regina
    q = cb.addQueen(24, 44)

    # una pedina bianca
    w = cb.addWhiteCM(40, 25)

    # una nera
    b = cb.addBlackCM(37, 57)

    # una bianca che rompe le scatole, posizionata relativamente alla
    # nera in modo che sia quasi a contatto formando un angolo di 240
    # gradi.
    ow = cb.addWhiteCM(b, 240, 2)

    # e uno striker
    s = cb.addStriker(6, "A")

    # manda in buca la pedina bianca, dopo un rimbalzo sulla sponda
    # opposta
    hitw = w.traceTo(BORDER_C, POCKET_B)

    # manda la regina in buca in alto a destra
    hitq = q.traceTo(BORDER_D, POCKET_C)

    # manda la bianca in buca in basso a destra
    hitb = b.traceTo(BORDER_B, POCKET_A)

    # colpisci con lo striker la pedina, rimbalza sulla sponda
    # opposta, poi prosegui in buca
    s.traceTo(hitw, hitq, BORDER_C, hitb)

    cb.exportEps("magic-shot")
