# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 29: Defence examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(3, 8)
w1 = cb.addWhiteCM(b1, 90, 4)
w2 = cb.addWhiteCM(8, 2.5)
s1 = cb.addStriker(1, 'B')

s1.traceTo(w2.traceTo(POCKET_BASE_LEFT), BORDER_FRONT,
           w1.traceTo(b1.traceTo(POCKET_BASE_LEFT)))

w3 = cb.addWhiteCM(33, 69)
b2 = cb.addBlackCM(67, 71)
w4 = cb.addWhiteCM(71.5, 66)
s2 = cb.addStriker(6, 'A')

s2.traceTo(w3.traceTo(BORDER_FRONT, POCKET_BASE_LEFT), BORDER_FRONT,
           (65,2), w4.traceTo(POCKET_FRONT_RIGHT))

cb.exportEps('fig29')
