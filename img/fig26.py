# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 26: Shot examples
# :Created:   Mon Aug 30 15:46:22 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_LEFT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_FRONT_LEFT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(14, 72)
s1 = cb.addStriker(0, 'S')

s1.traceTo(BORDER_LEFT, (9, 2), BORDER_RIGHT, w1.traceTo(POCKET_FRONT_LEFT))

cb.exportEps('fig26')
