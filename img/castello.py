# -*- coding: utf-8 -*-
# :Project:   CTA -- Break drawing
# :Created:   Fri Feb 27 02:29:45 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from pyx import canvas, trafo, unit
from board import BlackCarromMan, Queen, WhiteCarromMan

unit.set(uscale=7, wscale=0.5)

c = canvas.canvas()

q = Queen(c, 0, 0)

w1 = WhiteCarromMan(c, q, 90)
w2 = WhiteCarromMan(c, w1, 90)
w3 = WhiteCarromMan(c, q, -30)
w4 = WhiteCarromMan(c, w3, -30)
w5 = WhiteCarromMan(c, q, -150)
w6 = WhiteCarromMan(c, w5, -150)

b1 = BlackCarromMan(c, q, -90)
b2 = BlackCarromMan(c, q, 30)
b3 = BlackCarromMan(c, q, 150)

b4 = BlackCarromMan(c, b1, -150)
b5 = BlackCarromMan(c, b1, -30)

b6 = BlackCarromMan(c, b2, 90)
b7 = BlackCarromMan(c, b2, -30)

b8 = BlackCarromMan(c, b3, 90)
b9 = BlackCarromMan(c, b3, -150)

w7 = WhiteCarromMan(c, b1, -90)
w8 = WhiteCarromMan(c, b2, 30)
w9 = WhiteCarromMan(c, b3, 150)

cc = canvas.canvas()
cc.insert(c, [trafo.translate(7, 8)])

cc.writeEPSfile('castello')
