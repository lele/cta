# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 14: Turning examples
# :Created:   Sun Feb 29 19:44:08 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_BASE_RIGHT,
    POCKET_FRONT_LEFT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(19,32)
w1 = cb.addWhiteCM(b1,-55)
s1 = cb.addStriker(4,'B')
hit1 = w1.hitPoint(-55)
b1.traceTo((b1.radius/2,62),(9,74-b1.radius/2),(35,50))
w1.traceTo(POCKET_FRONT_LEFT)
s1.traceTo(hit1)

b2 = cb.addBlackCM(10,3)
w2 = cb.addWhiteCM(b2,40)
s2 = cb.addStriker(1.5,'A')
hit2 = w2.hitPoint(40)
b2.traceTo((7,b2.radius/2), (b2.radius/2,5), (15,20))
w2.traceTo(POCKET_BASE_LEFT)
s2.traceTo(hit2)

b3 = cb.addBlackCM(64,4.6)
w3 = cb.addWhiteCM(b3,161,5)
s3 = cb.addStriker(6.4,'C')
hit3 = w3.traceTo(POCKET_BASE_RIGHT)
s3.traceTo(hit3)

cb.exportEps('fig14')
