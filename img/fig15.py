# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 15: Second examples
# :Created:   Sun Feb 29 22:25:49 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_BASE,
    BORDER_LEFT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_BASE_RIGHT,
    POCKET_FRONT_LEFT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(4,13)
b1 = cb.addBlackCM(2,6)
b2 = cb.addBlackCM(8,2)
w2 = cb.addWhiteCM(27,5)
b3 = cb.addBlackCM(67,71)
b4 = cb.addBlackCM(71.8,5.5)
w3 = cb.addWhiteCM(66,9.5)

s1 = cb.addStriker(3.2,'A')
hit1 = w2.traceTo(BORDER_BASE, POCKET_FRONT_LEFT)
s1.traceTo(hit1)

s2 = cb.addStriker(1,'C')
hit2 = w1.traceTo(BORDER_LEFT, POCKET_BASE_RIGHT)
hit3 = b3.traceTo(BORDER_RIGHT, (64,60))
s2.traceTo(hit2, BORDER_LEFT, hit3)

s3 = cb.addStriker(10,'B')
hit4 = w3.traceTo(BORDER_RIGHT, (30,4))
s3.traceTo(hit4, BORDER_BASE, b4.hitPoint(-145))

cb.exportEps('fig15')
