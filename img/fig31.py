# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 31: Defence examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    BORDER_LEFT,
    CarromBoard,
    POCKET_FRONT_LEFT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(4, 10)
w1 = cb.addWhiteCM(b1, 78, 5)
s1 = cb.addStriker(1, 'A')

s1.traceTo(b1.traceTo(BORDER_LEFT, (15,2)), BORDER_LEFT,
           w1.traceTo((22, 25)))

w2 = cb.addWhiteCM(26, 55)
b2 = cb.addBlackCM(43, 51)
w3 = cb.addWhiteCM(50, 69)
s2 = cb.addStriker(4, 'B')

s2.traceTo(w2.traceTo(POCKET_FRONT_LEFT), BORDER_FRONT,
           b2.traceTo((43, 5)))

cb.exportEps('fig31')
