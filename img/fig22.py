# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 22: Shot examples
# :Created:   lun 30 ago 2004 15:46:22 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015 Lele Gaifax
#

from board import (
    BORDER_LEFT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_BASE_LEFT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(7, 2.5)
s1 = cb.addStriker(5, 'S')

s1.traceTo(BORDER_LEFT, (65, 72), BORDER_RIGHT, w1.traceTo(POCKET_BASE_LEFT))

cb.exportEps('fig22')
