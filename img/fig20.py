# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 20: Force examples
# :Created:   Mon Aug 30 15:46:22 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_BASE_LEFT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(63, 3)
b1 = cb.addBlackCM(32, 11.5)
q1 = cb.addQueen(b1, -4, 1.9)
b2 = cb.addBlackCM(q1, 4, 1.9)
s1 = cb.addStriker(6.3, 'B')

s1.traceTo((1, 20))
q1.traceTo(POCKET_BASE_LEFT)

cb.exportEps('fig20')
