# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 33: Advanced shot examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(1.59, 18)
b2 = cb.addBlackCM(13, 3)
w1 = cb.addWhiteCM(b1, 40, 1.9)
s1 = cb.addStriker(0, 'S')

w1.traceTo(POCKET_FRONT_RIGHT)
s1.traceTo(b1.hitPoint(-45))

cb.exportEps('fig33')
