# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 16: Slip examples
# :Created:   Mon Mar 1 13:47:31 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BOARD_WIDTH,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_BASE_RIGHT,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(1.7,11)
w2 = cb.addWhiteCM(20,BOARD_WIDTH-1.7)
w3 = cb.addWhiteCM(31,BOARD_WIDTH-1.7)
w4 = cb.addWhiteCM(50,BOARD_WIDTH-2.1)
w5 = cb.addWhiteCM(BOARD_WIDTH-1.7,20)
w6 = cb.addWhiteCM(53,1.7)

s1 = cb.addStriker(0,'D')
w1.traceTo(POCKET_BASE_LEFT)
s1.traceTo(w1.hitPoint(45))

s2 = cb.addStriker(5,'C')
w2.traceTo(POCKET_FRONT_LEFT)
s2.traceTo(w2.hitPoint(-45))

s3 = cb.addStriker(4,'E')
hit3 = w4.traceTo(POCKET_FRONT_RIGHT)
w3.traceTo((26,BOARD_WIDTH-1.7))
s3.traceTo(w3.hitPoint(-45),hit3)

s4 = cb.addStriker(7,'B')
w6.traceTo(POCKET_BASE_RIGHT)
s4.traceTo(w6.hitPoint(135))

s5 = cb.addStriker(10,'A')
w5.traceTo(POCKET_FRONT_RIGHT)
s5.traceTo(w5.hitPoint(-135))

cb.exportEps('fig16')
