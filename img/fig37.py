# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 37: Advanced shot examples
# :Created:   Wed Sep 08 15:36:44 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_LEFT,
    CarromBoard,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(24, 20)
w2 = cb.addWhiteCM(30, 22)
w3 = cb.addWhiteCM(31, 28)
w4 = cb.addWhiteCM(35, 32)
w5 = cb.addWhiteCM(40, 32.5)
w6 = cb.addWhiteCM(72, 7)
b1 = cb.addBlackCM(w3, -40)

s = cb.addStriker(3, 'S')
b1.traceTo(POCKET_BASE_RIGHT)
s.traceTo(BORDER_LEFT, w3.hitPoint(160))

cb.exportEps('fig37')
