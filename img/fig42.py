# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 42
# :Created:   Sat Dec 27 18:48:15 2008
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2008-2015 Lele Gaifax
#

from board import (
    BORDER_LEFT,
    CarromBoard,
    POCKET_B,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(3.9,24.0)
b1 = cb.addBlackCM(w1,15,5.3)
b2 = cb.addBlackCM(b1,140,5.5)
b3 = cb.addBlackCM(3.4,12,"a")
s1 = cb.addStriker(5,"A")

hit2 = w1.traceTo(BORDER_LEFT, POCKET_B)
hit1 = b1.traceTo(hit2)
s1.traceTo(hit1)

cb.exportEps("fig42")
