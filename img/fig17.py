# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 17: Bomb examples
# :Created:   Sat May 1 17:17:07 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BOARD_WIDTH,
    BORDER_BASE,
    BORDER_FRONT,
    BORDER_LEFT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(20.3, 10)
w2 = cb.addWhiteCM(32, 5)
w3 = cb.addWhiteCM(55, 11)
w4 = cb.addWhiteCM(34, 68.5)
w5 = cb.addWhiteCM(54, 30)

b1 = cb.addBlackCM(69.3, 57)

q = cb.addQueen(69.8, 35)

s1 = cb.addStriker(2, 'A')
s2 = cb.addStriker(5.5, 'B')
s3 = cb.addStriker(8.2, 'C')

w1.traceTo(POCKET_BASE_LEFT)
s1.traceTo(BORDER_LEFT, (65,BOARD_WIDTH-(4.13/2)), BORDER_RIGHT, (40,40))

hit2 = w2.traceTo(POCKET_BASE_LEFT)
hit4 = w4.traceTo(BORDER_FRONT, (25, 57))
s2.traceTo(hit2, BORDER_BASE, hit4)

w3.traceTo(POCKET_BASE_RIGHT)
hitb1 = b1.traceTo(BORDER_RIGHT, (70,BOARD_WIDTH-(3.18/2)), (55,46))
s3.traceTo(BORDER_BASE, hitb1)

cb.exportEps('fig17')
