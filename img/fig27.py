# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 27: Shot examples
# :Created:   Mon Aug 30 15:46:22 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_BASE,
    BORDER_FRONT,
    CarromBoard,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(33, 34)
s1 = cb.addStriker(7, 'S')

s1.traceTo(w1.traceTo(BORDER_FRONT, (2, 60), BORDER_BASE, POCKET_FRONT_RIGHT))

cb.exportEps('fig27')
