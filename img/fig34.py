# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 34: Advanced shot examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_LEFT,
    CarromBoard,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(1.59, 26)
w1 = cb.addWhiteCM(b1, -45, 2)
b2 = cb.addBlackCM(3, 13)
s1 = cb.addStriker(4, 'S')

w1.traceTo(BORDER_LEFT, POCKET_BASE_RIGHT)
s1.traceTo(w1.hitPoint(35))

cb.exportEps('fig34')
