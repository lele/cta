# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 19: Force examples
# :Created:   Mon Aug 30 15:25:32 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BOARD_WIDTH,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_FRONT_LEFT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(63, 3)
b1 = cb.addBlackCM(32, 11.5)
q1 = cb.addQueen(b1, 1, 1.8)
s1 = cb.addStriker(5.5, 'B')

s1.traceTo((16, 2))
q1.traceTo(POCKET_BASE_LEFT)
b1.traceTo((4, 11))

q2 = cb.addQueen(BOARD_WIDTH/2, BOARD_WIDTH/2)
b2 = cb.addBlackCM(q2, 90, 1.8)
b3 = cb.addBlackCM(q2, 180, 2)
s2 = cb.addStriker(10, 'A')

s2.traceTo(q2.traceTo(POCKET_FRONT_LEFT))

cb.exportEps('fig19')
