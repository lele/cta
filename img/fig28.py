# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 28: Defence examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(8, 72)
w1 = cb.addWhiteCM(b1, -5, 3)
s1 = cb.addStriker(0, 'A')

s1.hit((b1, 270))
b1.traceTo((3, 50))

b2 = cb.addBlackCM(72, 9)
w2 = cb.addWhiteCM(b2, 95, 3)
s2 = cb.addStriker(10, 'B')

s2.hit((b2, 180))
b2.traceTo((50, 3))

cb.exportEps('fig28')
