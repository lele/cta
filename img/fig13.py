# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 13: Third pocket examples
# :Created:   Sun Feb 29 01:43:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    BORDER_LEFT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_BASE_RIGHT,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(6,17)
s1 = cb.addStriker(0,'A')
hit1 = w1.traceTo(BORDER_LEFT, POCKET_FRONT_RIGHT)
s1.traceTo(hit1)

w2 = cb.addWhiteCM(39,40)
s2 = cb.addStriker(7,'C')
hit2 = w2.traceTo(BORDER_LEFT, POCKET_FRONT_RIGHT)
s2.traceTo(hit2)

w3 = cb.addWhiteCM(65,38)
w4 = cb.addWhiteCM(71.5,6.5)
b1 = cb.addBlackCM(71,66)
s3 = cb.addStriker(8.3,'B')
hit3 = w3.traceTo(BORDER_RIGHT, POCKET_FRONT_LEFT)
hit4 = w4.traceTo(POCKET_BASE_RIGHT)
s3.traceTo(hit3, BORDER_FRONT, hit4)

cb.exportEps('fig13')
