# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 11: Glance examples
# :Created:   Thu Feb 26 14:53:56 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(2.5,67)
b1 = cb.addBlackCM(18.2,29)
s1 = cb.addStriker(1.1,"A")

hit1 = b1.traceTo(BORDER_FRONT, (15,4))
hit2 = w1.traceTo(POCKET_FRONT_LEFT)
s1.traceTo(hit1,hit2)

w1 = cb.addWhiteCM(35,66.5)
w2 = cb.addWhiteCM(w1,175,6)
s1 = cb.addStriker(8, "D")

hit1 = w1.traceTo(BORDER_FRONT, (30,21))
hit2 = w2.traceTo(POCKET_FRONT_LEFT)
s1.traceTo(hit1,hit2)

b1 = cb.addBlackCM(51,58)
b2 = cb.addBlackCM(58,40)
w1 = cb.addWhiteCM(68.5,67.5)
s1 = cb.addStriker(3.5,"B")
s2 = cb.addStriker(10,"C")

hit1 = b1.traceTo(BORDER_FRONT, (46,4))
hit2 = w1.traceTo(POCKET_FRONT_RIGHT)
s1.traceTo(hit1,hit2)

hit3 = b2.traceTo(BORDER_FRONT, (53,5))
s2.traceTo(hit3,hit2)

cb.exportEps("fig11")
