# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 24: Shot examples
# :Created:   Mon Aug 30 15:46:22 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_BASE,
    BORDER_FRONT,
    CarromBoard,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(72, 7)
s1 = cb.addStriker(5, 'S')

s1.traceTo(BORDER_BASE, (2, 55), BORDER_FRONT, w1.traceTo(POCKET_BASE_RIGHT))

cb.exportEps('fig24')
