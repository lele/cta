# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 8: Other touch examples
# :Created:   Thu Feb 26 00:47:55 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(25,15)
b1 = cb.addBlackCM(21,18)
b2 = cb.addBlackCM(28,20)
s1 = cb.addStriker(5, "A")

hit1 = w1.hit((b1,-80), POCKET_BASE_LEFT)
s1.traceTo(hit1)

w2 = cb.addWhiteCM(35,35)
w4 = cb.addWhiteCM(w2,95,2)
b3 = cb.addBlackCM(w2,153,2)
b4 = cb.addBlackCM(w2,37,2)
s2 = cb.addStriker(9, "B")

w2.hit((b3,70), POCKET_FRONT_LEFT)
hit2 = b4.hitPoint(-15)
s2.traceTo(hit2)

b5 = cb.addBlackCM(68,62)
w5 = cb.addWhiteCM(68.5,48)
s3 = cb.addStriker(7, "C")

hit3 = w5.traceTo("B", (71,62), POCKET_FRONT_RIGHT)
s3.traceTo(hit3)

cb.exportEps("fig08")
