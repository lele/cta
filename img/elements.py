# -*- coding: utf-8 -*-
# :Project:   CTA -- Board elements drawing
# :Created:   Sun Feb 29 02:00:32 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import CarromBoard, CarromMan, BOARD_WIDTH

FRAME_WIDTH = 5

class DescriptiveCarromBoard(CarromBoard):

    def exportEps(self, filename):
        """Aggiungi il bordo del tavolo."""

        from pyx import path, trafo, canvas, style

        c = canvas.canvas()
        frame = path.rect(0, 0,
                          BOARD_WIDTH+2*FRAME_WIDTH, BOARD_WIDTH+2*FRAME_WIDTH)
        c.stroke(frame, [style.linewidth.THICK])
        c.insert(self.canvas, [trafo.translate(FRAME_WIDTH, FRAME_WIDTH)])
        c.writeEPSfile(filename)

class Element(CarromMan):
    """Un elemento"""

    radius = 2.8

    def __init__(self, c, x, y, l):
        """Costruttore di uno Striker."""

        self.canvas = c
        self.x = x
        self.y = y
        self.label = l
        self._draw()

    def _draw(self):
        """Disegna l'elemento."""

        from pyx import path, text, color, style

        c = path.circle(self.x, self.y, self.radius)
        self.canvas.fill(c, [color.gray.white])
        self.canvas.text(self.x, self.y, r"%s" % self.label,
                         [text.halign.center, text.vshift.middlezero])
        self.canvas.stroke(c, [style.linewidth.THICK])

elements = (
    ( (20,-2.5), 'A' ), # Sponda di base
    ( (50,76.5), 'B' ), # Sponda opposta
    ( (-2.5,20), 'C' ), # Sponda sinistra
    ( (76.5,50), 'D' ), # Sponda destra
    ( (15,10),   'E' ), # Cerchio di base sinistro
    ( (59,10),   'F' ), # Cerchio di base destro
    ( (15,64),   'G' ), # Cerchio di base opposto sinistro
    ( (59,64),   'H' ), # Cerchio di base opposto destro
    ( (30,11.5), 'I' ), # Linee di base
    ( (30,62),   'L' ), # Linee di base opposte
    ( (11.5,30), 'M' ), # Linee di base sinistre
    ( (62,30),   'N' ), # Linee di base destre
    ( (39,39),   'O' ), # Cerchio centrale
    ( (42,31),   'P' ), # Cerchio interno
    ( (49,40),   'Q' ), # Cerchio esterno
    ( (3,3),     'R' ), # Buca di base sinistra
    ( (71,3),    'S' ), # Buca di base destra
    ( (3,71),    'T' ), # Buca opposta sinistra
    ( (71,71),   'U' ), # Buca opposta destra
    ( (24,24),   'V' ), # Cerchio con freccia sinistro
    ( (51,24),   'W' ), # Cerchio con freccia destro
    ( (24,51),   'X' ), # Cerchio con freccia opposto sinistro
    ( (51,51),   'Y' ), # Cerchio con freccia opposto destro
    )

cb = DescriptiveCarromBoard()
for o,l in elements:
    Element(cb.canvas,o[0],o[1],l)

cb.exportEps('elements')
