# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 7: Touch examples
# :Created:   Thu Feb 26 00:09:05 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(4, 14)
w1 = cb.addWhiteCM(b1, 5, 5)
s1 = cb.addStriker(2, "A")

hit1 = w1.hit((b1,-25), POCKET_BASE_LEFT)
s1.traceTo(hit1)

w2 = cb.addWhiteCM(30, 55)
b2 = cb.addBlackCM(w2, 50)
s2 = cb.addStriker(4, "C")

w2.traceTo(POCKET_FRONT_LEFT)
hit2 = w2.hitPoint(-90)
s2.traceTo(hit2)

b3 = cb.addBlackCM(64,70)
b4 = cb.addBlackCM(71,62)
w3 = cb.addWhiteCM(67,51)
s3 = cb.addStriker(10, "B")

hit3 = w3.hit((b3,-45), POCKET_FRONT_RIGHT)
s3.traceTo(hit3)

cb.exportEps("fig07")
