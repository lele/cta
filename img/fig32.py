# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 32: Advanced shot examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_FRONT_LEFT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(5, 13)
w2 = cb.addWhiteCM(4, 18)
b1 = cb.addBlackCM(16, 16)
w3 = cb.addWhiteCM(21, 18)
s1 = cb.addStriker(1.4, 'S')

s1.traceTo(w3.traceTo(POCKET_FRONT_LEFT), (72, 63), BORDER_FRONT,
           (8, 19))

cb.exportEps('fig32')
