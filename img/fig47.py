# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 47
# :Created:   Mon Dec 29 01:27:44 2008
# :Created:   dom 08 nov 2015 18:24:43 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2008-2015 Lele Gaifax
#

from board import (
    BORDER_BASE,
    BORDER_LEFT,
    CarromBoard,
    POCKET_A,
    POCKET_B,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(21.1,10.3)
w2 = cb.addWhiteCM(55.1,10.3)
w3 = cb.addWhiteCM(37,68)
w4 = cb.addWhiteCM(35,5)
s1 = cb.addStriker(2.2,"A")
s2 = cb.addStriker(6.3,"B")
s3 = cb.addStriker(8.3,"C")

hit1 = w1.traceTo(POCKET_A)
hit2 = w2.traceTo(POCKET_B)
hit3 = w4.traceTo(POCKET_A)
s1.traceTo(BORDER_LEFT, (71.5,60), w3.hitPoint(0))
s2.traceTo(hit3, BORDER_BASE, w3.hitPoint(-90))
s3.traceTo(BORDER_BASE, (71.5,25), w3.hitPoint(-50))

cb.exportEps("fig47")
