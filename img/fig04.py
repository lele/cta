# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 4: Double, Cross Double
# :Created:   Fri Dec 12 12:05:21 2003
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2003-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    BORDER_LEFT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_BASE_RIGHT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(3, 67)

w1 = cb.addWhiteCM(10,55)
hit1 = w1.traceTo(BORDER_FRONT, POCKET_BASE_LEFT)

w2 = cb.addWhiteCM(35,70)
hit2 = w2.traceTo(POCKET_FRONT_RIGHT)

w3 = cb.addWhiteCM(63,20)
hit3 = w3.traceTo(BORDER_FRONT, POCKET_BASE_RIGHT)

b2 = cb.addBlackCM(71,62)

q = cb.addQueen(57,27)
hitq = q.traceTo((50, 29))

b3 = cb.addBlackCM(q, 160)
w4 = cb.addWhiteCM(b3, 260, 2)
w5 = cb.addWhiteCM(56,21)

s1 = cb.addStriker(3, "B")
s1.traceTo(hit1, BORDER_LEFT, hit2)

s2 = cb.addStriker(10, "A")
s2.traceTo(hit3, BORDER_RIGHT, hitq)

cb.exportEps("fig04")
