# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 43
# :Created:   Sun Dec 28 11:06:03 2008
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2008-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_D,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(5,71.5)
w2 = cb.addWhiteCM(3.1,17.0)
b1 = cb.addBlackCM(2.5,8.3)
b2 = cb.addBlackCM(21,34)
b3 = cb.addBlackCM(54,2.5)
s1 = cb.addStriker(2.6,"A")

hit3 = w2.traceTo((12,5))
hit2 = w1.traceTo(POCKET_D)
hit1 = b2.traceTo(BORDER_FRONT, hit3)
s1.traceTo(hit1, hit2)

cb.exportEps("fig43")
