# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 36: Advanced shot examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    BORDER_LEFT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(2.5, 8)
w2 = cb.addWhiteCM(16, 26)
w3 = cb.addWhiteCM(67, 71)

b1 = cb.addBlackCM(10, 47)
b2 = cb.addBlackCM(55, 42)
b3 = cb.addBlackCM(70, 12)

s1 = cb.addStriker(4, 'S')

s1.traceTo(b1.traceTo(BORDER_LEFT, w3.traceTo(POCKET_FRONT_RIGHT)),
           BORDER_FRONT, w1.traceTo(POCKET_BASE_LEFT))

cb.exportEps('fig36')
