# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 6: Press examples
# :Created:   Thu Feb 12 15:22:44 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(31.7, 38.3)
w2 = cb.addWhiteCM(32, 35)
hit1 = w1.traceTo(BORDER_FRONT, POCKET_BASE_LEFT)
hit2 = w2.traceTo(POCKET_FRONT_LEFT)

s1 = cb.addStriker(4, "A")
s1.traceTo(hit2)

b1 = cb.addBlackCM(42, 40.5)
w3 = cb.addWhiteCM(42, 37)
hit3 = w3.traceTo(POCKET_FRONT_RIGHT)

s2 = cb.addStriker(2, "B")
s2.traceTo(hit3)

cb.exportEps("fig06")
