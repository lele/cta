# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 44
# :Created:   Sun Dec 28 11:20:51 2008
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2008-2015 Lele Gaifax
#

from board import (
    BORDER_LEFT,
    CarromBoard,
    POCKET_B,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(7,12)
w2 = cb.addWhiteCM(68,13.2)
b1 = cb.addBlackCM(20,13.5)
b2 = cb.addBlackCM(1.9,4.5)
b3 = cb.addBlackCM(72,6)
s1 = cb.addStriker(2.8,"A")

hit3 = w2.traceTo((63,6))
hit2 = w1.traceTo(BORDER_LEFT, POCKET_B)
hit1 = b1.traceTo(hit2)

s1.traceTo(hit1, BORDER_LEFT, (24.5,71.7), hit3)

cb.exportEps("fig44")
