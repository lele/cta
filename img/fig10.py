# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 10: Pair examples
# :Created:   Thu Feb 26 14:42:42 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

cb = CarromBoard()

b1 = cb.addBlackCM(19,38)
b2 = cb.addBlackCM(b1,105,6)
s1 = cb.addStriker(1, "A")

hit2 = b2.traceTo(POCKET_FRONT_LEFT)
hit1 = b1.traceTo(hit2)
s1.traceTo(hit1)

b1 = cb.addBlackCM(34,51)
b2 = cb.addBlackCM(b1,135,5)
s1 = cb.addStriker(4.5, "B")

hit2 = b2.traceTo(POCKET_FRONT_LEFT)
hit1 = b1.traceTo(hit2)
s1.traceTo(hit1)

b1 = cb.addBlackCM(66,35)
b2 = cb.addBlackCM(b1,70,6)
s1 = cb.addStriker(9, "C")

hit2 = b2.traceTo(POCKET_FRONT_RIGHT)
hit1 = b1.traceTo(hit2)
s1.traceTo(hit1)

cb.exportEps("fig10")
