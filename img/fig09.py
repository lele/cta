# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 9: Shot examples
# :Created:   Thu Feb 26 01:14:52 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_FRONT_LEFT,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(17,38)
b2 = cb.addBlackCM(b1,95)
s1 = cb.addStriker(4, "A")

hit1 = b1.hitPoint(-10)
s1.traceTo(hit1)
b2.traceTo(POCKET_FRONT_LEFT)

b1 = cb.addBlackCM(38,56)
b2 = cb.addBlackCM(b1,165)
s1 = cb.addStriker(6, "C")

hit1 = b1.hitPoint(-75)
s1.traceTo(hit1)
b2.traceTo(POCKET_FRONT_LEFT)

b1 = cb.addBlackCM(56,39)
b2 = cb.addBlackCM(b1,60)
s1 = cb.addStriker(9, "B")

hit1 = b1.hitPoint(-90)
s1.traceTo(hit1)
b2.traceTo(POCKET_FRONT_RIGHT)

cb.exportEps("fig09")
