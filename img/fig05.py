# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 5: Cross double examples
# :Created:   Fri Dec 12 16:35:49 2003
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2003-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    BORDER_LEFT,
    CarromBoard,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(5,2)
w1 = cb.addWhiteCM(16,7)
hit1 = w1.traceTo(BORDER_LEFT, POCKET_BASE_RIGHT)

w2 = cb.addWhiteCM(39,42)
hit2 = w2.traceTo(BORDER_FRONT, POCKET_BASE_RIGHT)

w3 = cb.addWhiteCM(w2, 185)

s1 = cb.addStriker(1, "A")
s1.traceTo(hit2)

s2 = cb.addStriker(4, "C")
s2.traceTo(hit1)

cb.exportEps("fig05")
