# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 41
# :Created:   Fig Dec 26 20:29:26 2008
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2008-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_A,
    POCKET_B,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(23.5,69.0,"a",75)
w2 = cb.addWhiteCM(42.8,65.5,"b")
b1 = cb.addBlackCM(25.1,64.0)
b2 = cb.addBlackCM(53.1,67.1)
s1 = cb.addStriker(1,"A")

hit3 = w2.traceTo(POCKET_B)
hit2 = w1.traceTo(BORDER_FRONT, POCKET_A)
hit1 = b1.traceTo(hit2, BORDER_FRONT, (45.1,13.2))

s1.traceTo(hit1, BORDER_FRONT, hit3)

cb.exportEps("fig41")
