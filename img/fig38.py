# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 38: Other touch examples
# :Created:   Wed Apr 19 16:37:17 2006
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2006-2015 Lele Gaifax
#

from pyx import text
from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_C,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(30,46)
b1 = cb.addBlackCM(37,47.5)
b2 = cb.addBlackCM(2.5,67)
b3 = cb.addBlackCM(3,7)
s1 = cb.addStriker(7.1, "A")
s2 = cb.addStriker(8.5, "B")

hit = w1.traceTo("C", (13.5,2), (2.5,65))
cb.canvas.text(28,69, r"\bf a", [text.halign.center, text.vshift.middlezero])
s1.traceTo(hit, BORDER_FRONT, b2.hitPoint(55))

hit = w1.traceTo("C", (2.5,55), (34,2.5), POCKET_C)
cb.canvas.text(18.5,69, r"\bf b", [text.halign.center, text.vshift.middlezero])
s2.traceTo(hit)

cb.exportEps("fig38")
