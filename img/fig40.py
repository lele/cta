# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 40: Other touch examples
# :Created:   Wed Apr 21 23:37:17 2006
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2006-2015 Lele Gaifax
#

from board import (
    BORDER_LEFT,
    CarromBoard,
    POCKET_A,
    POCKET_B,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(11.8,14.8)
w2 = cb.addWhiteCM(69,13.5)
b1 = cb.addBlackCM(8,15.7)
s1 = cb.addStriker(3, "A")

hit1 = w1.traceTo(POCKET_A)
hit2 = w2.traceTo(POCKET_B)
s1.traceTo(hit1, BORDER_LEFT, (31,72), hit2)

cb.exportEps("fig40")
