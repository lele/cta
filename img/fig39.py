# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 39: Other touch examples
# :Created:   Wed Apr 19 16:37:17 2006
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2006-2015 Lele Gaifax
#

from board import (
    CarromBoard,
    POCKET_C,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(14.5,25)
w2 = cb.addWhiteCM(60,5)
b1 = cb.addBlackCM(27,3.5)
b2 = cb.addBlackCM(2.5,65)
b3 = cb.addBlackCM(3.2,8)
q1 = cb.addQueen(21,29)
s1 = cb.addStriker(2, "A")

hit1 = w1.traceTo("D", POCKET_C)
hit2 = q1.traceTo(POCKET_C)
s1.traceTo(hit1,hit2)

cb.exportEps("fig39")
