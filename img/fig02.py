# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 2: Cut, Straight Cut e Cross Cut
# :Created:   Thu Dec 11 12:36:51 2003
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2003-2015 Lele Gaifax
#

from board import (
    BOARD_WIDTH,
    BORDER_D,
    CarromBoard,
    POCKET_C,
    POCKET_D,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(25,27)
hit1 = w1.traceTo(POCKET_D)

w2 = cb.addWhiteCM(30,55)
hit2 = w2.traceTo(BORDER_D, (BOARD_WIDTH/2, BOARD_WIDTH-3))

w3 = cb.addWhiteCM(33,58)
hit3 = w3.traceTo(POCKET_D)

w4 = cb.addWhiteCM(55,45)
hit4 = w4.traceTo(POCKET_C)

s1 = cb.addStriker(3, "B")
s1.traceTo(hit1)

s2 = cb.addStriker(6, "D")
s2.traceTo(hit2)

s3 = cb.addStriker(7, "A")
s3.traceTo(hit3)

s4 = cb.addStriker(10, "C")
s4.traceTo(hit4)

cb.exportEps("fig02")
