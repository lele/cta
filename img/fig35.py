# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 35: Advanced shot examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from pyx import style
from board import (
    BORDER_LEFT,
    CarromBoard,
    POCKET_FRONT_RIGHT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(4.5, 71.5)
w1 = cb.addWhiteCM(17, 70)
b2 = cb.addBlackCM(10, 58)
s1 = cb.addStriker(4, 'S')

hitp = w1.traceTo(POCKET_FRONT_RIGHT)
s1.traceTo(BORDER_LEFT, hitp)

s1.linestyle = style.linestyle.dotted
s1.traceTo(BORDER_LEFT, (12, 72), hitp)

cb.exportEps('fig35')
