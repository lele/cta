# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 30: Defence examples
# :Created:   Mon Aug 30 17:11:41 2004
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2004-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    CarromBoard,
    POCKET_FRONT_LEFT,
    )

cb = CarromBoard()

b1 = cb.addBlackCM(30, 50)
w1 = cb.addWhiteCM(30, 72)
s1 = cb.addStriker(8, 'A')

s1.traceTo(b1.traceTo(BORDER_FRONT, (2,58), (45,4)), w1.hitPoint(-45))
w1.traceTo(POCKET_FRONT_LEFT)

b2 = cb.addBlackCM(65, 69)
s2 = cb.addStriker(4, 'B')
s2.traceTo(b2.traceTo(BORDER_FRONT, (71, 66), (35, 4)))

cb.exportEps('fig30')
