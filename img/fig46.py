# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 46
# :Created:   Mon Dec 29 01:08:34 2008
# :Created:   dom 08 nov 2015 18:23:49 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2008-2015 Lele Gaifax
#

from board import (
    BORDER_BASE,
    CarromBoard,
    POCKET_C,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(2,10.6)
b1 = cb.addBlackCM(1.8,7)
w2 = cb.addWhiteCM(71.5,67)
s1 = cb.addStriker(0,"A")

hit2 = w2.traceTo(POCKET_C)
b1.traceTo((18,7.1))
s1.traceTo(b1.hitPoint(0), BORDER_BASE, hit2)

cb.exportEps("fig46")
