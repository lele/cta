# -*- coding: utf-8 -*-
# :Project:   CTA -- Fig 3: Double, Cross Double
# :Created:   Thu Dec 11 12:36:51 2003
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2003-2015 Lele Gaifax
#

from board import (
    BORDER_FRONT,
    BORDER_RIGHT,
    CarromBoard,
    POCKET_BASE_LEFT,
    POCKET_BASE_RIGHT,
    )

cb = CarromBoard()

w1 = cb.addWhiteCM(20,35)
hit1 = w1.traceTo(BORDER_FRONT, POCKET_BASE_LEFT)

w2 = cb.addWhiteCM(35,67)
hit2 = w2.traceTo(BORDER_FRONT, POCKET_BASE_LEFT)

w3 = cb.addWhiteCM(59,52)
hit3 = w3.traceTo(BORDER_FRONT, POCKET_BASE_RIGHT)

b1 = cb.addBlackCM(6, 65)

q = cb.addQueen(70,49)

b2 = cb.addBlackCM(69.5, 62)
hitb2 = b2.traceTo((67,70))

s1 = cb.addStriker(2, "A")
s1.traceTo(hit1)

s2 = cb.addStriker(5, "C")
s2.traceTo(hit2)

s3 = cb.addStriker(7, "B")
s3.traceTo(hit3, BORDER_RIGHT, hitb2)

cb.exportEps("fig03")
