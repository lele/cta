# -*- coding: utf-8 -*-
# :Project:   CTA -- Main makefile
# :Created:   Thu Dec 26 22:08:03 2002
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2002-2015 Lele Gaifax
#

LYX := lyx

FIGURES_PY = $(wildcard img/fig??.py)
FIGURES_EPS := img/castello.eps img/elements.eps $(FIGURES_PY:%.py=%.eps)

all: $(FIGURES_EPS) cta.pdf

img/%.eps img/%.png: img/%.py img/board.py
	(cd img; python $*.py)

cta.pdf: cta.lyx regole.lyx preamble.tex
	$(LYX) -E pdf4 $@ cta.lyx

clean:
	rm -f *.toc *.aux *.idx *.lof *.log *.ilg *.ind *.out

realclean: clean
	rm -f *~ *.dvi *.ps *.pdf cta.tex regole.tex $(FIGURES_EPS)
